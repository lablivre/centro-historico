package br.org.chbelem;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import java.io.File;
import br.org.chbelem.data.DataSource;
import br.org.chbelem.data.DataSourceStorage;
import br.org.chbelem.gui.utils.Utilidades;
import br.org.chbelem.map.Mapasimvida;
import br.org.chbelem.gui.utils.AlarmReceiver;



public class MainSeam extends Activity {
	private PendingIntent pendingIntent;
	private MixViewDataHolder mixViewData;
	protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lablivre);
		DataSourceStorage.init(this);
                /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(MainSeam.this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainSeam.this, 0, alarmIntent, 0);

        String dir = Environment.getExternalStorageDirectory() + "/centrohistorico";
        File f = new File(dir);
        if(!f.isDirectory()) {
            File newdir = new File(dir);
            newdir.mkdirs();
        }
        iniciar();

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}
	public void abrir(View view){

		DataSourceStorage.getInstance();
		DataSourceStorage.getInstance().clear();
		DataSource newDs = new DataSource("Mapas", "http://forumlandi.org/centrohistorico/requestapp.php",
		DataSource.TYPE.values()[3],
		DataSource.DISPLAY.values()[1], true, null, null);
		DataSourceStorage.getInstance().add(newDs);
		startActivity(new Intent(this, MixView.class));
		
	}
	public void abrirmapa(View view){
		startActivity(new Intent(this, Mapasimvida.class));

	}

	public void reclamar(View view){
		startActivity(new Intent(this, Registrar.class));

	}
	public void iniciar(){
		Context context = this;
		Intent alarmIntent = new Intent(this, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		int interval = 60* 1000 * 30; //30 minutos
		manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
	}

	public void qrcode(View view) {
        startActivity(new Intent(this, DecoderActivity.class));
	}


}

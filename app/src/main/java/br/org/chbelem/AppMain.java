package br.org.chbelem;

/*import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;*/
import android.app.Application;
import android.os.Environment;

import java.io.File;

/**
 * This class is for debugging purpose, it initializes ACRA (Application Crash
 * Report for Android) which sends the stack trace to this google Form:
 *  {@link https://docs.google.com/spreadsheet/ccc?key=0AkBr1EPcS_mfdFdFNXpjUExsNi1rRTJNc095LTh6RGc#gid=0}
 * 
 * @author KlemensE
 */
//@ReportsCrashes(formKey = "dFdFNXpjUExsNi1rRTJNc095LTh6RGc6MQ")
public class AppMain extends com.orm.SugarApp {
	
	private static AppMain instancia;
	
	public AppMain(){
		instancia = this;
	}
	public static AppMain getInstancia(){
		return instancia;
	}
	
	@Override
	public void onCreate() {
		// The following line triggers the initialization of ACRA
		/*if (BuildConfig.DEBUG) {
			ACRA.init(this);
		}*/

		super.onCreate();
	}
}
package br.org.chbelem.plugin;

public enum PluginStatus {
	New,
	Deinstalled,
	Activated,
	Deactivated
}

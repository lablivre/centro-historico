/*
 * Copyright (C) 2010- Peer internet solutions
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */

package br.org.chbelem.marker;
import org.mixare.lib.MixUtils;
import org.mixare.lib.gui.PaintScreen;
import org.mixare.lib.gui.TextObj;
import org.mixare.lib.marker.draw.DrawImage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path;
import android.location.Location;
import br.org.chbelem.AppMain;
import br.org.chbelem.R;
/**
 * This markers represent the points of interest.
 * On the screen they appear as circles, since this
 * class inherits the draw method of the Marker.
 * 
 * @author hannes
 * 
 */
public class POIMarker extends LocalMarker{
	
	private Bitmap image;
	public static final int MAX_OBJECTS = 20;
	public static final int OSM_URL_MAX_OBJECTS = 5;
	private boolean isDirectionMarker = true;
	private Context ctx;

	public POIMarker(String id, String title, double latitude, double longitude,
			double altitude, String URL, int type, int color, String icone) {
		super(id, title, latitude, longitude, altitude, URL, type, color,icone);
		ctx = AppMain.getInstancia();
		// adicina icone personalizado
		if(icone != null){
			try{
				//final java.net.URL imageURI = new java.net.URL (icone);
				
				//this.setImage(BitmapFactory.decodeStream(imageURI.openConnection().getInputStream()));
				this.setImage(BitmapFactory.decodeResource(ctx.getResources(),R.drawable.ico));
				//this.setImage(getBitmapFromAsset("ico.png"));
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public boolean isDirectionMarker() {
		return isDirectionMarker;
	}

	public void setIsDirectionMarker(boolean direction) {
		this.isDirectionMarker = direction;
	}
	
	@Override
	public void update(Location curGPSFix) {
		super.update(curGPSFix);
	}

	@Override
	public int getMaxObjects() {
		return MAX_OBJECTS;
	}

	@Override
	public void drawCircle(PaintScreen dw) {
		if (isVisible) {
			drawImage(dw);
		}
	}
	
	int gc;
	@Override
	public void drawTextBlock(PaintScreen dw) {
		float maxHeight = Math.round(dw.getHeight() / 10f) + 1;
		// TODO: change textblo ck only when distance changes

		String textStr = "";

		if (isDirectionMarker) {
			double d = distance;
			textStr = getTitle() + "(" + MixUtils.formatDist((float) d) + ")";
		} else {
			textStr = getTitle();
		}
		textBlock = new TextObj(textStr, Math.round(maxHeight / 2f) + 1, 250, dw, isUnderline());
		
		if (isVisible) {
			// based on the distance set the colour
			if (distance < 100.0) {
				textBlock.setBgColor(Color.argb(128, 52, 52, 52));
				textBlock.setBorderColor(Color.rgb(255, 104, 91));
			} else {
				textBlock.setBgColor(Color.argb(128, 0, 0, 0));
				textBlock.setBorderColor(Color.rgb(255, 255, 255));
			}
			//dw.setColor(DataSource.getColor(type));

			float currentAngle = MixUtils.getAngle(cMarker.x, cMarker.y,
					signMarker.x, signMarker.y);
			txtLab.prepare(textBlock);
			dw.setStrokeWidth(1f);
			dw.setFill(true);
			dw.paintObj(txtLab, signMarker.x - txtLab.getWidth() / 2,
					signMarker.y + maxHeight, currentAngle + 90, 1);
			
		}
		gc++;
		if(gc>50){
			gc=0;
			System.gc();
			
		}
		
	}
	
	public void drawImage(final PaintScreen dw) {
		final DrawImage Image = new DrawImage(isVisible, cMarker, image);
		Image.draw(dw);
	}
	
	public void drawCicle(final PaintScreen dw){
		float maxHeight = dw.getHeight();
		dw.setStrokeWidth(maxHeight / 100f);
		dw.setFill(false);

			dw.setColor(getColor());
		
		// draw circle with radius depending on distance
		// 0.44 is approx. vertical fov in radians
		double angle = 2.0 * Math.atan2(10, distance);
		double radius = Math.max(
				Math.min(angle / 0.44 * maxHeight, maxHeight),
				maxHeight / 25f);
		
		/*
		 * distance 100 is the threshold to convert from circle to another
		 * shape
		 */
		if (distance < 100.0)
			otherShape(dw);
		else
			dw.paintCircle(cMarker.x, cMarker.y, (float) radius);
	}
	
	public void otherShape(PaintScreen dw) {
		// This is to draw new shape, triangle
		float currentAngle = MixUtils.getAngle(cMarker.x, cMarker.y,
				signMarker.x, signMarker.y);
		float maxHeight = Math.round(dw.getHeight() / 10f) + 1;

		dw.setColor(getColor());
		float radius = maxHeight / 1.5f;
		dw.setStrokeWidth(dw.getHeight() / 100f);
		dw.setFill(false);

		Path tri = new Path();
		float x = 0;
		float y = 0;
		tri.moveTo(x, y);
		tri.lineTo(x - radius, y - radius);
		tri.lineTo(x + radius, y - radius);

		tri.close();
		dw.paintPath(tri, cMarker.x, cMarker.y, radius * 2, radius * 2,
				currentAngle + 90, 1);
	}
	/**
	 * @param Bitmap the image to set
	 */
	public void setImage(Bitmap image) {
		this.image = image;
	}
}

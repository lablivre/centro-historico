package br.org.chbelem.gui.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.org.chbelem.MainSeam;
import br.org.chbelem.R;
import br.org.chbelem.data.models.Denuncias;

/**
 * Created by Leandro Cunha on 29/04/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    String upLoadServerUri = "http://forumlandi.org/centrohistorico/index.php/component/requestapp?format=raw";
    String cod = "";
    Context context;
    int i;
    boolean del;
    String updated = "";
    String uuid;
    @Override
    public void onReceive(Context ctx, Intent intent) {
        context = ctx;
        ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInf = conMgr.getAllNetworkInfo();
        NetworkInfo internet = conMgr.getActiveNetworkInfo();
        uuid = Settings.Secure.getString(ctx.getContentResolver(),Settings.Secure.ANDROID_ID);
        Log.i("UUID", uuid);
        if (internet!=null && internet.isConnected()) {
            enviar(ctx);
            push(ctx);
        }

        }
    public void push(Context ctx){
        Ion.with(ctx, "http://forumlandi.org/centrohistorico/notifica.php")
                .setMultipartParameter("phonid",uuid)
                .asString().withResponse().
                setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> request) {
                        String result = request.getResult();
                        ArrayList<String> itens;
                        try {
                            JSONObject jo = new JSONObject(result);
                            JSONArray ja;
                            ja = jo.getJSONArray("results");
                            itens = new ArrayList<String>();
                            for (int i = 0; i < ja.length(); i++) {
                                notifica(context, ja.getJSONObject(i).getString("title")
                                        , ja.getJSONObject(i).getString("text"));
                            }

                        } catch (Exception ejson) {
                            ejson.printStackTrace();
                        }
                    }
                });
    }

    private static void notifica(Context context, String title, String message) {

        Intent principal = new Intent(context, MainSeam.class);
        PendingIntent actv = PendingIntent.getActivity(context, 0, principal, Intent.FLAG_ACTIVITY_NEW_TASK);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                // Set Icon
                .setSmallIcon(R.drawable.icon_datasource)
                        // Set Ticker Message
                .setTicker(message)
                        // Set Title
                .setContentTitle(title)
                        // Set Text
                .setContentText(message)
                .setSound(uri).setContentIntent(actv)
                .setVibrate(new long[]{500, 500, 500})
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }

    private void enviar(Context ctx) {


                System.out.println("Conectado");
                List<Denuncias> den = Denuncias.listAll(Denuncias.class);
                int densize = den.size();
                if (densize > 0) {
                    for (i = 0; i < den.size(); i++) {
                        {
                            cod = Utilidades.randInt(100000000, 999999999) + "";
                            try {
                                File imagem = new File(den.get(i).getUrlfile());
                                if (!imagem.isFile()) {
                                    den.get(i).delete();
                                    Toast.makeText(ctx, "Imagem não encontrada, faca o registro novamente", Toast.LENGTH_LONG);
                                    break;
                                }
                                updated = den.get(i).getId().toString();
                                Ion.with(ctx, upLoadServerUri)
                                        .setMultipartParameter("text_registro", "Registro N.")
                                        .setMultipartParameter("gera_numm", cod)
                                        .setMultipartParameter("imagem_hidden", "images/")
                                        .setMultipartParameter("pdi", "[" + den.get(i).getLat() + "," + den.get(i).getLng() + "]")
                                        .setMultipartParameter("lng", den.get(i).getLng())
                                        .setMultipartParameter("lat", den.get(i).getLat())
                                        .setMultipartParameter("task", "save")
                                        .setMultipartParameter("id", "0")
                                        .setMultipartParameter("fm_", "1")
                                        .setMultipartParameter("config[type]", "cadastro_de_pois")
                                        .setMultipartParameter("config[stage]", "-1")
                                        .setMultipartParameter("config[skip]", "0")
                                        .setMultipartParameter("config[url]", "http://forumlandi.org/centrohistorico/index.php/registro-sobre-acidentes")
                                        .setMultipartParameter("config[id]", "0")
                                        .setMultipartParameter("detalhes", den.get(i).getDetalhes())
                                        .setMultipartParameter("config[itemId]", "105")
                                        .setMultipartParameter("config[unique]", "seblod_form_cadastro_de_pois")
                                        .setMultipartFile("imagem", new File(den.get(i).getUrlfile())).
                                        asString().withResponse().setCallback(
                                        new FutureCallback<Response<String>>() {
                                            @Override
                                            public void onCompleted(Exception arg0,
                                                                    Response<String> r) {
                                                if (r.getResult().equalsIgnoreCase("ok")) {
                                                    System.out.println("PROCESSADO OK: " + updated);

                                                }
                                            }
                                        }
                                );

                                System.out.println("Loop: " + i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            den.get(i).delete();
                        }

                    }
                }


    }
}

/**
 * 
 */
package br.org.chbelem.gui.sectionedlist;

/**
 * This class holds the information whether a row is a section or a entry
 * @author KlemensE
 */
public interface Item {
	
	public boolean isSection();
}

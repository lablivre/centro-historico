/*
 * Copyright (c) 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 
 
 
 JSON EXAMPLE
 
[
    {
        "name": "John Doe",
        "latlng": [
            62.457434,
            17.349869
        ],
        "population": "123"
    },
    {
        "name": "Jane Doe",
        "latlng": [
            62.455102,
            17.345599
        ],
        "population": "132"
    },
    {
        "name": "James Bond",
        "latlng": [
            62.458287,
            17.356306
        ],
        "population": "123"
    }
] 
 */

package br.org.chbelem.map;

import br.org.chbelem.R;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author saxman
 */
public class GoogleMapsV2 extends FragmentActivity {
}
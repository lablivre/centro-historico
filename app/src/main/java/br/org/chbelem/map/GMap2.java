package br.org.chbelem.map;


import br.org.chbelem.MixListView;
import br.org.chbelem.MixView;
import br.org.chbelem.R;
import org.mixare.lib.MixUtils;
import org.mixare.lib.marker.Marker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koushikdutta.ion.Ion;


public class GMap2 extends SherlockGoogleMapActivity implements
OnTouchListener, ActionBar.OnNavigationListener  {
	
	private GoogleMap map;
	private Context ctx = this;
	private String[] mapas;
	public static final String PREFS_NAME = "MixMapPrefs";
	
	/* Menu ID's */
	// Center my Position
	//private static final int MENU_CENTER_POSITION_ID = Menu.FIRST;
	// Whether to display Satellite or Map
	private static final int MENU_CHANGE_MODE_ID = Menu.FIRST + 1;
	// Go to MixListView
	private static final int MENU_LIST_VIEW = Menu.FIRST + 2;
	// Go to AugmentedView
	private static final int MENU_CAMERA_VIEW = Menu.FIRST + 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_test_gmap);

		mapas = getResources().getStringArray(R.array.maps);
		
		LatLng mylatlng = new LatLng(MixView.getDataView().getContext().getLocationFinder()
				.getCurrentLocation().getLatitude(), MixView.getDataView().getContext().getLocationFinder()
				.getCurrentLocation().getLongitude());
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(mylatlng, setZoom()));
		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
		
		
		

			@Override
			public void onInfoWindowClick(
					com.google.android.gms.maps.model.Marker marker) {
				String url = MixUtils.parseAction(marker.getSnippet());
				
				try {
					if (url != null) {
						//String newUrl = MixUtils.parseAction(url);
						//Log.i("test", "open: " + url);
						MixView.getDataView().getContext().getWebContentManager()
						.loadWebPage(url,ctx);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		});
		//Context context = getSupportActionBar().getThemedContext();
		/*ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(
				context, R.array.maps, R.layout.sherlock_spinner_item);
		list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);*/

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		//getSupportActionBar().setSelectedNavigationItem(getOwnListPosition());
		//getSupportActionBar().setListNavigationCallbacks(list, this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	
		Marker marker;
		int limit = MixView.getDataView().getDataHandler().getMarkerCount();
		
		for (int i = 0; i < limit; i++) {
			marker = MixView.getDataView().getDataHandler().getMarker(i);
						
			map.addMarker(new MarkerOptions()
				.position(new LatLng(marker.getLatitude(),marker.getLongitude()))
					.title(marker.getTitle()).snippet(marker.getURL()));
			
		}

	
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		

		// TODO: Get Strings out of values
		SubMenu subMenu1 = menu.addSubMenu("More");
		
		if (map.getMapType()==2) {
			subMenu1.add(MENU_CHANGE_MODE_ID, MENU_CHANGE_MODE_ID, Menu.NONE,
					"Mudar para mapa")
					.setIcon(android.R.drawable.ic_menu_mapmode);
		} else {
			subMenu1.add(MENU_CHANGE_MODE_ID, MENU_CHANGE_MODE_ID, Menu.NONE,
					"Mudar para satélite")
					.setIcon(android.R.drawable.ic_menu_mapmode);
		}
		
		subMenu1.add(MENU_LIST_VIEW, MENU_LIST_VIEW, Menu.NONE, "Lista")
				.setIcon(android.R.drawable.ic_menu_view);

		subMenu1.add(MENU_CAMERA_VIEW, MENU_CAMERA_VIEW, Menu.NONE,
				getString(R.string.map_menu_cam_mode))
				.setIcon(android.R.drawable.ic_menu_camera);
		
		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.abs__ic_menu_moreoverflow_holo_dark);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Actionbar icon pressed
		case android.R.id.home:
			finish();
			break;
		/* MapMode */
		case MENU_CHANGE_MODE_ID:
			if (map.getMapType()==2) {
				map.setMapType(1);
			} else {
				map.setMapType(2);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			break;
		
		/* List View */
		case MENU_LIST_VIEW:
			createListView();
			// finish(); don't close map if list view created
			break;
		/* back to Camera View */
		case MENU_CAMERA_VIEW:
			closeMapViewActivity(false);
			break;
		default:
			break;// do nothing

		}
		return true;
	}

	
	private void createListView() {
		if (MixView.getDataView().getDataHandler().getMarkerCount() > 0) {
			Intent intent1 = new Intent(this, MixListView.class);
			intent1.setAction(Intent.ACTION_VIEW);
			startActivityForResult(intent1, 42);// TODO receive result if any!
		}
		/* if the list is empty */
		else {
			MixView.getDataView().getContext().getNotificationManager()
					.addNotification(getString(R.string.empty_list));
		}
	}
	
	private void closeMapViewActivity(boolean doRefreshScreen) {
		Intent closeMapView = new Intent();
		closeMapView.putExtra("RefreshScreen", doRefreshScreen);
		setResult(RESULT_OK, closeMapView);
		finish();
	}
	
	private int setZoom() {
		float mapZoomLevel = (MixView.getDataView().getRadius() / 2f);
		mapZoomLevel = MixUtils
				.earthEquatorToZoomLevel((mapZoomLevel < 2f) ? 2f
						: mapZoomLevel);
		return (int) mapZoomLevel;
	}


	

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}




	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		/*if (mapas[itemPosition].equals(getString(R.string.map_menu_map_osm))) {
			MixMap.changeMap(MixMap.MAPS.OSM);
			Intent intent = new Intent(this, OsmMap.class);
			startActivity(intent);
			finish();
		}*/
		return true;
	}




	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
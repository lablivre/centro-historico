package br.org.chbelem.map;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import br.org.chbelem.R;
import br.org.chbelem.gui.utils.Utilidades;

public class Mapasimvida extends SherlockActivity implements OnMapReadyCallback{
    private String re;
    GoogleMap map;
    private Context ctx = this;
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    protected LocationManager locationManager;
    double lat = 0,lng =0;
    Utilidades util = new Utilidades();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gmap);

        if(!util.checkConn(this)){
            message(this,"Você não está conectado à internet.");
        }else{
            //showCurrentLocation();
        }

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());

            double distancia = util.gps2m(lat, lng, location.getLatitude(),location.getLongitude());

            if(distancia>5) {
                lat = location.getLatitude();
                lng = location.getLongitude();
                LatLng center = new LatLng(lat, lng);
                carregamapa(lat,lng);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 13));
            }

        }
    };


    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        map.setOnMyLocationChangeListener(myLocationChangeListener);
        map.setMyLocationEnabled(true);

        LatLng center = new LatLng(lat, lng);
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 13));

        map.setOnInfoWindowClickListener(
                new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        try {
                            util.loadWebPage(marker.getSnippet(), Mapasimvida.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

    }
    @Override
    protected void onResume(){
        super.onResume();
    }

    protected void carregamapa(double lat,double lng){
        try {
            if (util.checkConn(this)&& lat != 0 && lng != 0) {
                final ProgressDialog dialog = ProgressDialog.show(this, "", "Carregando mapa...", true);
                Ion.with(Mapasimvida.this, "http://forumlandi.org/centrohistorico/requestapp.php")
                        .setBodyParameter("lat", lat + "")
                        .setBodyParameter("lng", lng + "")
                        .setBodyParameter("raio", "20").asString().withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> request) {
                                String result = request.getResult();
                                try {
                                    JSONObject jo = new JSONObject(result);
                                    if (result != "") {
                                        dialog.dismiss();
                                        JSONArray ja;
                                        ja = jo.getJSONArray("results");
                                        for (int i = 0; i < ja.length(); i++) {
                                            LatLng mk = new LatLng(ja.getJSONObject(i).getDouble("lat"), ja.getJSONObject(i).getDouble("lng"));
                                            map.addMarker(new MarkerOptions()
                                                    .title(ja.getJSONObject(i).getString("title"))
                                                    .snippet(ja.getJSONObject(i).getString("webpage"))
                                                    .position(mk));
                                        }
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    dialog.dismiss();
                                    finish();
                                }
                            }
                        });
            }
            }catch(Exception e){
                e.printStackTrace();
            }

    }
    public void message(final Activity activity,String message){
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setPositiveButton("Fechar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                finish();
                                d.dismiss();
                            }
                        });
        builder.create().show();

    }
}

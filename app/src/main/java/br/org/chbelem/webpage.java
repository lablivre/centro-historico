package br.org.chbelem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class webpage extends Activity {

    private WebView myWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webpage);

        myWebView = (WebView) findViewById(R.id.webview);

        Intent i = getIntent();
        String url = (String) i.getSerializableExtra("Webpage");

        myWebView.loadUrl(url);
    }

}

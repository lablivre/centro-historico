package br.org.chbelem;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.org.chbelem.gui.utils.Utilidades;


public class Listadepontos extends SherlockActivity{
    ListView listview ;
    ArrayList<String> itens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listadepontos);
        //String[] itens = new String[]{"asda", "asdasd", "asdasd","asdadsasd"};
        carregamapa();
        listview = (ListView) findViewById(R.id.listpontos);




    }
    protected ArrayList<String> carregamapa(){
        try {
            if (Utilidades.checkConn(this)) {
                final ProgressDialog dialog = ProgressDialog.show(this, "", "Carregando lista...", true);
                //Log.i("JSON",jo.getString("results"));
                Ion.with(this, "http://forumlandi.org/centrohistorico/requestapp.php")
                        .setBodyParameter("lat", "-1.4")
                        .setBodyParameter("lng", "-48.5")
                        .setBodyParameter("raio", "200000").asString().withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> request) {
                                String result = request.getResult();
                                try{
                                    JSONObject jo = new JSONObject(result);
                                    JSONArray ja;
                                    ja = jo.getJSONArray("results");
                                    itens = new ArrayList<String>();
                                    for(int i=0; i < ja.length();i++){
                                        itens.add(i,ja.getJSONObject(i).getString("title"));
                                    }
                                    dialog.dismiss();
                                    setList();
                                }catch (Exception ejson){
                                    ejson.printStackTrace();
                                }
                            }
                        });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return itens;
    }
    public void setList(){
        ArrayAdapter<String> adaptes = new ArrayAdapter<String>(
                this,android.R.layout.simple_list_item_1,itens);
        listview.setAdapter(adaptes);
    }
}

package br.org.chbelem.data.models;

import com.orm.SugarRecord;

/**
 * Created by lablivre02 on 27/04/15.
 */

public class Denuncias extends SugarRecord<Denuncias> {
    String lat, lng, upLoadServerUri, detalhes, urlfile;
    public Denuncias(){

    }
    public Denuncias(String lat, String lng, String detalhes, String urlfile){
        this.detalhes = detalhes;
        this.lat = lat;
        this.lng = lng;
        this.urlfile = urlfile;
    }
    public String getDetalhes(){
        return this.detalhes;
    }
    public String getUrlfile(){
        return  this.urlfile;
    }
    public String getLat(){
        return this.lat;
    }
    public String getLng(){
        return this.lng;
    }
}

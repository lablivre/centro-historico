package br.org.chbelem;

import static android.hardware.SensorManager.SENSOR_DELAY_GAME;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.mixare.lib.gui.PaintScreen;
import org.mixare.lib.render.Matrix;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import br.org.chbelem.R.drawable;
import br.org.chbelem.data.DataSourceList;
import br.org.chbelem.data.DataSourceStorage;
import br.org.chbelem.map.MixMap;
import br.org.chbelem.mgr.HttpTools;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
//import android.util.FloatMath;

/**
 * This class is the main application which uses the other classes for different
 * functionalities.
 * It sets up the camera screen and the augmented screen which is in front of the
 * camera screen.
 * It also handles the main sensor events, touch events and location events.
 */
public class MixView extends SherlockActivity implements SensorEventListener, OnTouchListener {

	private CameraSurface camScreen;
	private AugmentedView augScreen;

	private boolean isInited;
	private static boolean isBackground;
	private static PaintScreen dWindow;
	private static DataView dataView;
	private boolean fError;

	/* Different error messages */
	protected static final int UNSUPPORTET_HARDWARE = 0;
	protected static final int GPS_ERROR = 1;
	public static final int GENERAL_ERROR = 2;
	protected static final int NO_NETWORK_ERROR = 4;
	
	// test
	public static boolean drawTextBlock = true;
	
	//----------
    private MixViewDataHolder mixViewData  ;
	
	/** TAG for logging */
	public static final String TAG = "Seam";
	public static MixView ctxs;
	// why use Memory to save a state? MixContext? activity lifecycle?
	// private static MixView CONTEXT;

	/** string to name & access the preference file in the internal storage */
	public static final String PREFS_NAME = "MyPrefsFileForMenuItems";

	/**
	 * Main application Launcher.
	 * Does:
	 * - Lock Screen.
	 * - Initiate Camera View
	 * - Initiate view {@link br.org.chbelem.DataView#draw(PaintScreen) DataView}
	 * - Initiate ZoomBar {@link android.widget.SeekBar SeekBar widget}
	 * - Display License Agreement if app first used.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// MixView.CONTEXT = this;
		
		try {
			isBackground = false;			
			handleIntent(getIntent());
			
			final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			getMixViewData().setmWakeLock(
					pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK,
							"My Tag"));
			
			getMixViewData().setSensorMgr(
					(SensorManager) getSystemService(SENSOR_SERVICE));
			
			killOnError();
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			if (getSupportActionBar() != null) {
				getSupportActionBar().hide();
			}

			maintainCamera();
			maintainAugmentR();
			maintainZoomBar();

			if (!isInited) {
				setdWindow(new PaintScreen());
				setDataView(new DataView(getMixViewData().getMixContext()));

				/* set the radius in data view to the last selected by the user */
				setZoomLevel();
				refreshDownload();
				isInited = true;
			}

			/*
			 * Get the preference file PREFS_NAME stored in the internal memory
			 * of the phone
			 */
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

			/* check if the application is launched for the first time */
			if (settings.getBoolean("firstAccess", false) == false) {
				firstAccess(settings);

			}

		} catch (Exception ex) {
			doError(ex, GENERAL_ERROR);
		}
	}

	public MixViewDataHolder getMixViewData() {
		if (mixViewData == null && isBackground == false) {
			// TODO: VERY important, only one!
			mixViewData = new MixViewDataHolder(new MixContext(this));
		}
		return mixViewData;
	}

	/**
	 * Part of Android LifeCycle that gets called when "Activity" MixView is
	 * being navigated away. <br/>
	 * Does: - Release Screen Lock - Unregister Sensors.
	 * {@link android.hardware.SensorManager SensorManager} - Unregister
	 * Location Manager. {@link br.org.chbelem.mgr.location.LocationFinder
	 * LocationFinder} - Switch off Download Thread.
	 * {@link br.org.chbelem.mgr.downloader.DownloadManager DownloadManager} -
	 * Cancel view refresh Timer. <br/>
	 * {@inheritDoc}
	 */
	@Override
	protected void onPause() {
		super.onPause();
		try {
			this.getMixViewData().getmWakeLock().release();
			camScreen.surfaceDestroyed(null);
			try {
				getMixViewData().getSensorMgr().unregisterListener(this,
						getMixViewData().getSensorGrav());
				getMixViewData().getSensorMgr().unregisterListener(this,
						getMixViewData().getSensorMag());
				getMixViewData().getSensorMgr().unregisterListener(this,
						getMixViewData().getSensorGyro());
				getMixViewData().getSensorMgr().unregisterListener(this);
				getMixViewData().setSensorGrav(null);
				getMixViewData().setSensorMag(null);
				getMixViewData().setSensorGyro(null);

				getMixViewData().getMixContext().getLocationFinder()
						.switchOff();
				getMixViewData().getMixContext().getDownloadManager()
						.switchOff();

				getMixViewData().getMixContext().getNotificationManager()
						.setEnabled(false);
				getMixViewData().getMixContext().getNotificationManager()
						.clear();
				if (getDataView() != null) {
					getDataView().cancelRefreshTimer();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (fError) {
				finish();
			}
		} catch (Exception ex) {
			doError(ex, GENERAL_ERROR);
		}
	}

	/**
	 * Mixare Activities Pipe message communication.
	 * Receives results from other launched activities
	 * and base on the result returned, it either refreshes screen or not.
	 * Default value for refreshing is false
	 * <br/>
	 * {@inheritDoc}
	 */
	protected void onActivityResult(final int requestCode,
			final int resultCode, Intent data) {
		//Log.d(TAG + " WorkFlow", "MixView - onActivityResult Called");
		// check if the returned is request to refresh screen (setting might be
		// changed)
		
			if (resultCode == 1 && requestCode == 35) {
				final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

				dialog.setTitle(R.string.launch_plugins);
				dialog.setMessage(R.string.plugins_changed);
				dialog.setCancelable(false);
				
				// Allways activate new plugins
				
//				final CheckBox checkBox = new CheckBox(ctx);
//				checkBox.setText(R.string.remember_this_decision);
//				dialog.setView(checkBox);		
				
				dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int whichButton) {
						startActivity(new Intent(getMixViewData().getMixContext().getApplicationContext(),
								PluginLoaderActivity.class));
						finish();
					}
				});

				dialog.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int whichButton) {
						d.dismiss();
					}
				});

				dialog.show();
			}

		try {
			if (data.getBooleanExtra("RefreshScreen", false)) {
				Log.d(TAG + " WorkFlow",
						"MixView - Received Refresh Screen Request .. about to refresh");
				repaint();
				setZoomLevel();
				refreshDownload();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Part of Android LifeCycle that gets called when "MixView" resumes.
	 * <br/>
	 * Does:
	 * - Acquire Screen Lock
	 * - Refreshes Data and Downloads
	 * - Initiate four Matrixes that holds user's rotation view.
	 * - Re-register Sensors. {@link android.hardware.SensorManager SensorManager}
	 * - Re-register Location Manager. {@link br.org.chbelem.mgr.location.LocationFinder LocationFinder}
	 * - Switch on Download Thread. {@link br.org.chbelem.mgr.downloader.DownloadManager DownloadManager}
	 * - restart view refresh Timer. 
	 * <br/>
	 * {@inheritDoc}
	 * 
	 */
	@Override
	protected void onResume() {
		super.onResume();
		isBackground = false;
		try {
			this.getMixViewData().getmWakeLock().acquire();
			killOnError();
			getMixViewData().getMixContext().doResume(this);

			HttpTools.setContext(getMixViewData().getMixContext());
			
			//repaint(); //repaint when requested
			setZoomLevel();
			getDataView().doStart();
			getDataView().clearEvents();
			getMixViewData().getMixContext().getNotificationManager().setEnabled(true);
			refreshDownload();
			
			getMixViewData().getMixContext().getDataSourceManager().refreshDataSources();

			float angleX, angleY;

			int marker_orientation = -90;

			int rotation = Compatibility.getRotation(this);

			// display text from left to right and keep it horizontal
			angleX = (float) Math.toRadians(marker_orientation);
			getMixViewData().getM1().set(1f, 0f, 0f, 0f,
					(float) java.lang.Math.cos(angleX),
					(float) -java.lang.Math.sin(angleX), 0f,
					(float) java.lang.Math.sin(angleX),
					(float) java.lang.Math.cos(angleX));
			angleX = (float) Math.toRadians(marker_orientation);
			angleY = (float) Math.toRadians(marker_orientation);
			if (rotation == 1) {
				getMixViewData().getM2().set(1f, 0f, 0f, 0f,
						(float) java.lang.Math.cos(angleX),
						(float) -java.lang.Math.sin(angleX), 0f,
						(float) java.lang.Math.sin(angleX),
						(float) java.lang.Math.cos(angleX));
				getMixViewData().getM3().set((float) java.lang.Math.cos(angleY), 0f,
						(float) java.lang.Math.sin(angleY), 0f, 1f, 0f,
						(float) -java.lang.Math.sin(angleY), 0f,
						(float) java.lang.Math.cos(angleY));
			} else {
				getMixViewData().getM2().set((float) java.lang.Math.cos(angleX), 0f,
						(float) java.lang.Math.sin(angleX), 0f, 1f, 0f,
						(float) -java.lang.Math.sin(angleX), 0f,
						(float) java.lang.Math.cos(angleX));
				getMixViewData().getM3().set(1f, 0f, 0f, 0f,
						(float) java.lang.Math.cos(angleY),
						(float) -java.lang.Math.sin(angleY), 0f,
						(float) java.lang.Math.sin(angleY),
						(float) java.lang.Math.cos(angleY));

			}

			getMixViewData().getM4().toIdentity();

			for (int i = 0; i < getMixViewData().getHistR().length; i++) {
				getMixViewData().getHistR()[i] = new Matrix();
			}

			getMixViewData().addListSensors(getMixViewData().getSensorMgr().getSensorList(
					Sensor.TYPE_ACCELEROMETER));
			if (getMixViewData().getSensor(0).getType() == Sensor.TYPE_ACCELEROMETER ) {
				getMixViewData().setSensorGrav(getMixViewData().getSensor(0));
			}//else report error (unsupported hardware)

			getMixViewData().addListSensors(getMixViewData().getSensorMgr().getSensorList(
					Sensor.TYPE_MAGNETIC_FIELD));
			if (getMixViewData().getSensor(1).getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				getMixViewData().setSensorMag(getMixViewData().getSensor(1));
			}//else report error (unsupported hardware)
			
			if (!getMixViewData().getSensorMgr().getSensorList(Sensor.TYPE_GYROSCOPE).isEmpty()){
				getMixViewData().addListSensors(getMixViewData().getSensorMgr().getSensorList(
						Sensor.TYPE_GYROSCOPE));
				if (getMixViewData().getSensor(2).getType() == Sensor.TYPE_GYROSCOPE) {
					getMixViewData().setSensorGyro(getMixViewData().getSensor(2));
				}
				getMixViewData().getSensorMgr().registerListener(this,
						getMixViewData().getSensorGyro(), SENSOR_DELAY_GAME);
			}
			
				getMixViewData().getSensorMgr().registerListener(this,
						getMixViewData().getSensorGrav(), SENSOR_DELAY_GAME);
				getMixViewData().getSensorMgr().registerListener(this,
						getMixViewData().getSensorMag(), SENSOR_DELAY_GAME);
				
			try {
				GeomagneticField gmf = getMixViewData().getMixContext()
						.getLocationFinder().getGeomagneticField();
				angleY = (float) Math.toRadians(-gmf.getDeclination());
				getMixViewData().getM4().set((float) java.lang.Math.cos(angleY), 0f,
						(float) java.lang.Math.sin(angleY), 0f, 1f, 0f,
						(float) -java.lang.Math.sin(angleY), 0f,
						(float) java.lang.Math.cos(angleY));
			} catch (Exception ex) {
				doError(ex, GPS_ERROR);
			}

			if (!isNetworkAvailable()) {
				Log.d("test", "no network");
				doError(null, NO_NETWORK_ERROR);
			} else {
				Log.d("test", "network");
			}
			
			getMixViewData().getMixContext().getDownloadManager().switchOn();
			getMixViewData().getMixContext().getLocationFinder().switchOn();
		} catch (Exception ex) {
			doError(ex, GENERAL_ERROR);
			try {
				if (getMixViewData().getSensorMgr() != null) {
					getMixViewData().getSensorMgr().unregisterListener(this,
							getMixViewData().getSensorGrav());
					getMixViewData().getSensorMgr().unregisterListener(this,
							getMixViewData().getSensorMag());
					getMixViewData().getSensorMgr().unregisterListener(this,
							getMixViewData().getSensorGyro());
					getMixViewData().setSensorMgr(null);
				}

				if (getMixViewData().getMixContext() != null) {
					getMixViewData().getMixContext().getLocationFinder()
							.switchOff();
					getMixViewData().getMixContext().getDownloadManager()
							.switchOff();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}finally{
			//This does not conflict with registered sensors (sensorMag, sensorGrav)
			//This is a place holder to API returned listed of sensors, we registered
			//what we need, the rest is unnecessary.
			getMixViewData().clearAllSensors();
		}

		Log.d("Log", "resume");
		if (getDataView().isFrozen()
				&& getMixViewData().getSearchNotificationTxt() == null) {
			getMixViewData().setSearchNotificationTxt(new TextView(this));
			getMixViewData().getSearchNotificationTxt().setWidth(
					getdWindow().getWidth());
			getMixViewData().getSearchNotificationTxt().setPadding(10, 2, 0, 0);
			getMixViewData().getSearchNotificationTxt().setText(
					getString(R.string.search_active_1) + " "
							+ DataSourceList.getDataSourcesStringList()
							+ getString(R.string.search_active_2));
			getMixViewData().getSearchNotificationTxt().setBackgroundColor(
					Color.DKGRAY);
			getMixViewData().getSearchNotificationTxt().setTextColor(
					Color.WHITE);

			getMixViewData().getSearchNotificationTxt()
					.setOnTouchListener(this);
			addContentView(getMixViewData().getSearchNotificationTxt(),
					new LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));
		} else if (!getDataView().isFrozen()
				&& getMixViewData().getSearchNotificationTxt() != null) {
			getMixViewData().getSearchNotificationTxt()
					.setVisibility(View.GONE);
			getMixViewData().setSearchNotificationTxt(null);
		}
	}

	/**
	 * Customize Activity after switching back to it.
	 * Currently it maintain and ensures view creation.
	 * <br/>
	 * {@inheritDoc}
	 */
	protected void onRestart() {
		super.onRestart();
		maintainCamera();
		maintainAugmentR();
		maintainZoomBar();
	}
	
	/**
	 * {@inheritDoc}
	 * Deallocate memory and stops threads.
	 * Please don't rely on this function as it's killable, 
	 * and might not be called at all.
	 */
	protected void onDestroy(){
		try{
			
			getMixViewData().getMixContext().getDownloadManager().shutDown();
			getMixViewData().getSensorMgr().unregisterListener(this);
			isBackground = true; //used to enforce garbage MixViewDataHolder
			getMixViewData().setSensorMgr(null);
			mixViewData = null;
			/*
			 * Invoked when the garbage collector has detected that this
			 * instance is no longer reachable. The default implementation does
			 * nothing, but this method can be overridden to free resources.
			 * 
			 * Do we have to create our own finalize?
			 */
			//finalize();
		}catch(Exception e){
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}finally{
			super.onDestroy();
		}
	}
	
	/* ********* Operators ***********/ 
	/**
	 * View Repainting.
	 * It deletes viewed data and initiate new one. {@link br.org.chbelem.DataView DataView}
	 */
	public void repaint() {
		// clear stored data
		getDataView().clearEvents();
		setDataView(null); //It's smelly code, but enforce garbage collector 
							//to release data.
		setDataView(new DataView(getMixViewData().getMixContext()));
		setdWindow(new PaintScreen());
		
	}

	/**
	 * Checks camScreen, if it does not exist, it creates one.
	 */
	private void maintainCamera() {
		if (camScreen == null) {
			camScreen = new CameraSurface(this);
		}
		setContentView(camScreen);
	}

	/**
	 * Checks augScreen, if it does not exist, it creates one.
	 */
	private void maintainAugmentR() {
		if (augScreen == null) {
			augScreen = new AugmentedView(this);
		}
		addContentView(augScreen, new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
	}

	/**
	 * Creates a zoom bar and adds it to view.
	 */
	private void maintainZoomBar() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		FrameLayout frameLayout = createZoomBar(settings);
		addContentView(frameLayout, new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
				Gravity.BOTTOM));
	}

	/**
	 * Refreshes Download TODO refresh downloads
	 */
	public void refreshDownload(){
		getMixViewData().getMixContext().getDownloadManager().switchOn();
		Log.i("MIX","isInited");
//		try {
//			if (getMixViewData().getDownloadThread() != null){
//				if (!getMixViewData().getDownloadThread().isInterrupted()){
//					getMixViewData().getDownloadThread().interrupt();
//					getMixViewData().getMixContext().getDownloadManager().restart();
//				}
//			}else { //if no download thread found
//				getMixViewData().setDownloadThread(new Thread(getMixViewData()
//						.getMixContext().getDownloadManager()));
//				//@TODO Syncronize DownloadManager, call Start instead of run.
//				mixViewData.getMixContext().getDownloadManager().run();
//			}
//		}catch (Exception ex){
//		}
	}
	
	/**
	 * Refreshes Viewed Data.
	 */
	public void refresh(){
		dataView.refresh();
	}

	public void setErrorDialog(int error) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		switch (error) {
		case NO_NETWORK_ERROR:
			builder.setMessage(getString(R.string.connection_error_dialog));
			break;
		case GPS_ERROR:
			builder.setMessage(getString(R.string.gps_error_dialog));
			break;
		case GENERAL_ERROR:
			builder.setMessage(getString(R.string.general_error_dialog));
			break;
		case UNSUPPORTET_HARDWARE:
			builder.setMessage(getString(R.string.unsupportet_hardware_dialog));
			break;
		}

		/*Retry*/
		builder.setPositiveButton(R.string.connection_error_dialog_button1, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// "restart" mixare
				startActivity(new Intent(getMixViewData().getMixContext().getApplicationContext(),
						PluginLoaderActivity.class));
				finish();
			}
		});
		if (error == GPS_ERROR) {
			/* Open settings */
			builder.setNeutralButton(R.string.connection_error_dialog_button2,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							try {
								Intent intent1 = new Intent(
										Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivityForResult(intent1, 42);
							} catch (Exception e) {
								Log.d(TAG, "No Location Settings");
							}
						}
					});
		} else if (error == NO_NETWORK_ERROR) {
			builder.setNeutralButton(R.string.connection_error_dialog_button2,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							try {
								Intent intent1 = new Intent(
										Settings.ACTION_DATA_ROAMING_SETTINGS);
								ComponentName cName = new ComponentName(
										"com.android.phone",
										"com.android.phone.Settings");
								intent1.setComponent(cName);
								startActivityForResult(intent1, 42);
							} catch (Exception e) {
								Log.d(TAG, "No Network Settings");
							}
						}
					});
		}
		/*Close application*/
		builder.setNegativeButton(R.string.connection_error_dialog_button3, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				finish();
			}
		});
		
		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Calculate Zoom Level base 80.
	 * Mixare support zooming between 0-80 and default value of 20,
	 * {@link android.widget.SeekBar SeekBar} on the other hand, is 0-100 base.
	 * This method handles the Zoom level conversion between Mixare ZoomLevel and SeekBar.
	 * 
	 * @return int Zoom Level base 80 
	 */
	public float calcZoomLevel(){

		int myZoomLevel = getMixViewData().getMyZoomBar().getProgress();
		float myout = 5;

		if (myZoomLevel <= 26) {
			myout = myZoomLevel / 25f;
		} else if (25 < myZoomLevel && myZoomLevel < 50) {
			myout = 1 + (myZoomLevel - 25) * 0.38f;
		} else if (25 == myZoomLevel) {
			myout = 1;
		} else if (50 == myZoomLevel) {
			myout = 10;
		} else if (50 < myZoomLevel && myZoomLevel < 75) {
			myout = 10 + (myZoomLevel - 50) * 0.83f;
		} else {
			myout = 30 + (myZoomLevel - 75) * 2f;
		}

		return myout;
	}

	/**
	 * Handle First time users. It display license agreement and store user's
	 * acceptance.
	 * 
	 * @param settings
	 */
	private void firstAccess(SharedPreferences settings) {
		SharedPreferences.Editor editor = settings.edit();
		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setMessage(getString(R.string.license));
		builder1.setNegativeButton(getString(R.string.close_button),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert1 = builder1.create();
		alert1.setTitle(getString(R.string.license_title));
		alert1.show();
		editor.putBoolean("firstAccess", true);

		// value for maximum POI for each selected OSM URL to be active by
		// default is 5
		editor.putInt("osmMaxObject", 5);
		editor.commit();

		// add the default datasources to the preferences file
		DataSourceStorage.getInstance().fillDefaultDataSources();
	}

	/**
	 * Create zoom bar and returns FrameLayout. FrameLayout is created to be
	 * hidden and not added to view, Caller needs to add the frameLayout to
	 * view, and enable visibility when needed.
	 * 
	 * @param SharedOreference settings where setting is stored
	 * @return FrameLayout Hidden Zoom Bar
	 */
	private FrameLayout createZoomBar(SharedPreferences settings) {
		getMixViewData().setMyZoomBar(new SeekBar(this));
		getMixViewData().getMyZoomBar().setMax(100);
		getMixViewData().getMyZoomBar().setProgress(
				settings.getInt("zoomLevel", 65));
		getMixViewData().getMyZoomBar().setOnSeekBarChangeListener(
				myZoomBarOnSeekBarChangeListener);
		getMixViewData().getMyZoomBar().setVisibility(View.INVISIBLE);

		FrameLayout frameLayout = new FrameLayout(this);

		frameLayout.setMinimumWidth(3000);
		LayoutParams pa = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		frameLayout.setLayoutParams(pa);
		frameLayout.addView(getMixViewData().getMyZoomBar());
		frameLayout.setPadding(10, 0, 10, 10);
		return frameLayout;
	}

	/**
	 * Checks whether a network is available or not
	 * @return True if connected, false if not
	 */
	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}
	
	/* ********* Operator - Menu ***** */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		int base = Menu.FIRST;
		/* define the first */
		//MenuItem item1 = menu.add(base, base, base,
		//		getString(R.string.menu_item_1));
		MenuItem item2 = menu.add(base, base + 1, base + 1,
				getString(R.string.menu_item_2));
		MenuItem item3 = menu.add(base, base + 2, base + 2,
				getString(R.string.menu_item_3));
		MenuItem item4 = menu.add(base, base + 3, base + 3,
				getString(R.string.menu_item_4));
		MenuItem item5 = menu.add(base, base + 4, base + 4,
				getString(R.string.menu_item_5));
		MenuItem item6 = menu.add(base, base + 5, base + 5,
				getString(R.string.menu_item_6));
		MenuItem item7 = menu.add(base, base + 6, base + 6,
				getString(R.string.menu_item_7));
		MenuItem item8 = menu.add(base, base + 7, base + 7,
				getString(R.string.menu_item_8));
		
		/* assign icons to the menu items */
		//item1.setIcon(drawable.icon_datasource);
		item2.setIcon(drawable.icon_datasource);
		item3.setIcon(android.R.drawable.ic_menu_view);
		item4.setIcon(android.R.drawable.ic_menu_mapmode);
		item5.setIcon(android.R.drawable.ic_menu_zoom);
		item6.setIcon(android.R.drawable.ic_menu_search);
		item7.setIcon(android.R.drawable.ic_menu_info_details);
		item8.setIcon(android.R.drawable.ic_menu_share);

		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		/* Data sources */
		case 1:
			if (!getDataView().getIsLauncherStarted()) {
				Intent intent = new Intent(MixView.this, DataSourceList.class);
				startActivityForResult(intent, 40);
			} else {
				dataView.getContext().getNotificationManager()
					.addNotification(getString(R.string.no_website_available));
			}
			break;
			/* Plugin View */
		case 2:
			if (!getDataView().getIsLauncherStarted()) {
				Intent intent = new Intent(MixView.this,
						PluginListActivity.class);
				startActivityForResult(intent, 35);
			} else {
				dataView.getContext().getNotificationManager()
					.addNotification(getString(R.string.no_website_available));
			}
			break;
		/* List view */
		case 3:
			/*
			 * if the list of titles to show in alternative list view is not
			 * empty
			 */
			if (getDataView().getDataHandler().getMarkerCount() > 0) {
				Intent intent1 = new Intent(MixView.this, MixListView.class); 
				intent1.setAction(Intent.ACTION_VIEW);
				startActivityForResult(intent1, 42);
			}
			/* if the list is empty */
			else {
				dataView.getContext().getNotificationManager().
				addNotification(getString(R.string.empty_list));
			}
			break;
		/* Map View */
		case 4:
			Intent intent2 = new Intent(MixView.this, MixMap.class);
			startActivityForResult(intent2, 20);
			break;
		/* zoom level */
		case 5:
			getMixViewData().getMyZoomBar().setVisibility(View.VISIBLE);
			getMixViewData().setZoomProgress(
					getMixViewData().getMyZoomBar().getProgress());
			break;
		/* Search */
		case 6:
			onSearchRequested();
			break;
		/* GPS Information */
		case 7:
			Location currentGPSInfo = getMixViewData().getMixContext()
					.getLocationFinder().getCurrentLocation();
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.general_info_text) + "\n\n"
					+ getString(R.string.longitude)
					+ currentGPSInfo.getLongitude() + "\n"
					+ getString(R.string.latitude)
					+ currentGPSInfo.getLatitude() + "\n"
					+ getString(R.string.altitude)
					+ currentGPSInfo.getAltitude() + "m\n"
					+ getString(R.string.speed) + currentGPSInfo.getSpeed()
					+ "km/h\n" + getString(R.string.accuracy)
					+ currentGPSInfo.getAccuracy() + "m\n"
					+ getString(R.string.gps_last_fix)
					+ new Date(currentGPSInfo.getTime()).toString() + "\n");
			builder.setNegativeButton(getString(R.string.close_button),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.setTitle(getString(R.string.general_info_title));
			alert.show();
			break;
		/* Case 6: license agreements */
		case 8:
			AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
			builder1.setMessage(getString(R.string.license));
			/* Retry */
			builder1.setNegativeButton(getString(R.string.close_button),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			AlertDialog alert1 = builder1.create();
			alert1.setTitle(getString(R.string.license_title));
			alert1.show();
			break;
		}
		return true;
	}

	/* ******** Operators - Sensors ****** */
	private SeekBar.OnSeekBarChangeListener myZoomBarOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			float myout = calcZoomLevel();

			getMixViewData().setZoomLevel(String.valueOf(myout));
			getMixViewData().setZoomProgress(progress);

			dataView.getContext().getNotificationManager().
			addNotification("Radius: " + String.valueOf(myout));
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			dataView.getContext().getNotificationManager().addNotification("Radius: ");
		}

		public void onStopTrackingTouch(SeekBar seekBar) {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			/* store the zoom range of the zoom bar selected by the user */
			editor.putInt("zoomLevel", seekBar.getProgress());
			editor.commit();
			getMixViewData().getMyZoomBar().setVisibility(View.INVISIBLE);
			// zoomChanging= false;

			getMixViewData().getMyZoomBar().setProgress(seekBar.getProgress());

			dataView.getContext().getNotificationManager().clear();
			//repaint after zoom level changed.
			repaint();
			setZoomLevel();
			refreshDownload();
			
		}

	};

	public void onSensorChanged(SensorEvent evt) {
		try {
			if (getMixViewData().getSensorGyro() != null) {
				
				if (evt.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
					getMixViewData().setGyro(evt.values);
				}
				
				if (evt.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
					getMixViewData().setGrav(
							getMixViewData().getGravFilter().lowPassFilter(evt.values,
									getMixViewData().getGrav()));
				} else if (evt.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
					getMixViewData().setMag(
							getMixViewData().getMagFilter().lowPassFilter(evt.values,
									getMixViewData().getMag()));
				}
				getMixViewData().setAngle(
						getMixViewData().getMagFilter().complementaryFilter(
								getMixViewData().getGrav(),
								getMixViewData().getGyro(), 30,
								getMixViewData().getAngle()));
				
				SensorManager.getRotationMatrix(
						getMixViewData().getRTmp(),
						getMixViewData().getI(), 
						getMixViewData().getGrav(),
						getMixViewData().getMag());
			} else {
				if (evt.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
					getMixViewData().setGrav(evt.values);
				} else if (evt.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
					getMixViewData().setMag(evt.values);
				}
				SensorManager.getRotationMatrix(
						getMixViewData().getRTmp(),
						getMixViewData().getI(), 
						getMixViewData().getGrav(),
						getMixViewData().getMag());
			}
			
			augScreen.postInvalidate();

			int rotation = Compatibility.getRotation(this);

			if (rotation == 1) {
				SensorManager.remapCoordinateSystem(getMixViewData().getRTmp(),
						SensorManager.AXIS_X, SensorManager.AXIS_MINUS_Z,
						getMixViewData().getRot());
			} else {
				SensorManager.remapCoordinateSystem(getMixViewData().getRTmp(),
						SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_Z,
						getMixViewData().getRot());
			}
			getMixViewData().getTempR().set(getMixViewData().getRot()[0],
					getMixViewData().getRot()[1], getMixViewData().getRot()[2],
					getMixViewData().getRot()[3], getMixViewData().getRot()[4],
					getMixViewData().getRot()[5], getMixViewData().getRot()[6],
					getMixViewData().getRot()[7], getMixViewData().getRot()[8]);

			getMixViewData().getFinalR().toIdentity();
			getMixViewData().getFinalR().prod(getMixViewData().getM4());
			getMixViewData().getFinalR().prod(getMixViewData().getM1());
			getMixViewData().getFinalR().prod(getMixViewData().getTempR());
			getMixViewData().getFinalR().prod(getMixViewData().getM3());
			getMixViewData().getFinalR().prod(getMixViewData().getM2());
			getMixViewData().getFinalR().invert();
			
			getMixViewData().getHistR()[getMixViewData().getrHistIdx()]
					.set(getMixViewData().getFinalR());
			
			int histRLenght = getMixViewData().getHistR().length;
			
			getMixViewData().setrHistIdx(getMixViewData().getrHistIdx() + 1);
			if (getMixViewData().getrHistIdx() >= histRLenght)
				getMixViewData().setrHistIdx(0);

			getMixViewData().getSmoothR().set(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f,
					0f);
			for (int i = 0; i < histRLenght; i++) {
				getMixViewData().getSmoothR().add(
						getMixViewData().getHistR()[i]);
			}
			getMixViewData().getSmoothR().mult(
					1 / (float) histRLenght);

			getMixViewData().getMixContext().updateSmoothRotation(
					getMixViewData().getSmoothR());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent me) {
		if (getMixViewData().getMyZoomBar().getVisibility() == View.VISIBLE) {
			getMixViewData().getMyZoomBar().setVisibility(View.INVISIBLE);
		}
		
		try {
			killOnError();

			float xPress = me.getX();
			float yPress = me.getY();
			if (me.getAction() == MotionEvent.ACTION_UP) {
				getDataView().clickEvent(xPress, yPress);
			}// TODO add gesture events (low)

			return true;
		} catch (Exception ex) {
			// doError(ex);
			ex.printStackTrace();
			return super.onTouchEvent(me);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		try {
			killOnError();
			
			if (getMixViewData().getMyZoomBar().getVisibility() == View.VISIBLE) {
				getMixViewData().getMyZoomBar().setVisibility(View.INVISIBLE);
				if (keyCode == KeyEvent.KEYCODE_MENU) {
					return super.onKeyDown(keyCode, event);
				}
				return true;
			}

			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if (getDataView().isDetailsView()) {
					getDataView().keyEvent(keyCode);
					getDataView().setDetailsView(false);
					return true;
				} else {
					Intent close = new Intent();
					close.putExtra("closed", "MixView");
					setResult(0, close);
					finish();
					return super.onKeyDown(keyCode, event);
				}
			} else if (keyCode == KeyEvent.KEYCODE_MENU) {
				return super.onKeyDown(keyCode, event);
			} else {
				getDataView().keyEvent(keyCode);
				return false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return super.onKeyDown(keyCode, event);
		}
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD
				&& accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE
				&& getMixViewData().getCompassErrorDisplayed() == 0) {
			for (int i = 0; i < 2; i++) {
				dataView.getContext().getNotificationManager().
				addNotification("Bússola dados pouco confiáveis. Por favor, recalibrar bússola.");
			}
			getMixViewData().setCompassErrorDisplayed(
					getMixViewData().getCompassErrorDisplayed() + 1);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		getDataView().setFrozen(false);
		if (getMixViewData().getSearchNotificationTxt() != null) {
			getMixViewData().getSearchNotificationTxt()
					.setVisibility(View.GONE);
			getMixViewData().setSearchNotificationTxt(null);
		}
		return true;
	}

	/* ************ Handlers ************ */

	public void doError(Exception ex1, int error) {
		if (!fError) {
//			fError = true;

			setErrorDialog(error);

			try {
				ex1.printStackTrace();
			} catch (Exception ex2) {
				ex2.printStackTrace();
			}
		}

		try {
			augScreen.invalidate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void killOnError() throws Exception {
		if (fError)
			throw new Exception();
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			intent.setClass(this, MixListView.class);
			startActivity(intent);
			}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}


	/* ******* Getter and Setters ********** */

	public boolean isZoombarVisible() {
		return getMixViewData().getMyZoomBar() != null
				&& getMixViewData().getMyZoomBar().getVisibility() == View.VISIBLE;
	}

	public String getZoomLevel() {
		return getMixViewData().getZoomLevel();
	}

	/**
	 * @return the dWindow
	 */
	public static PaintScreen getdWindow() {
		return dWindow;
	}

	/**
	 * @param dWindow
	 *            the dWindow to set
	 */
	static void setdWindow(PaintScreen dWindow) {
		MixView.dWindow = dWindow;
	}

	/**
	 * @return the dataView
	 */
	public static DataView getDataView() {
		return dataView;
	}

	/**
	 * @param dataView
	 *            the dataView to set
	 */
	static void setDataView(DataView dataView) {
		MixView.dataView = dataView;
	}

	public int getZoomProgress() {
		return getMixViewData().getZoomProgress();
	}

	public void setZoomLevel() {
		float myout = calcZoomLevel();

		getDataView().setRadius(myout);
		getMixViewData().setZoomLevel(String.valueOf(myout));
		//caller has the to control of zoombar visibility, not setzoom
		//mixViewData.getMyZoomBar().setVisibility(View.INVISIBLE);
		//mixViewData.setZoomLevel(String.valueOf(myout));
		//setZoomLevel, caller has to call refreash download if needed.
//		mixViewData.setDownloadThread(new Thread(mixViewData.getMixContext().getDownloadManager()));
//		mixViewData.getDownloadThread().start();

	}

}

/**
 * @author daniele
 * 
 */
class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {
	MixView app;
	SurfaceHolder holder;
	Camera camera;

	CameraSurface(Context context) {
		super(context);
		try {
			app = (MixView) context;

			holder = getHolder();
			holder.addCallback(this);
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
			holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		try {
			if (camera != null) {
				try {
					camera.stopPreview();
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					camera.release();
				} catch (Exception e) {
					e.printStackTrace();
				}
				camera = null;
			}

			camera = Camera.open();
			camera.setPreviewDisplay(holder);
		} catch (Exception ex) {
			try {
				if (camera != null) {
					try {
						camera.stopPreview();
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						camera.release();
					} catch (Exception e) {
						e.printStackTrace();
					}
					camera = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		try {
			if (camera != null) {
				try {
					camera.stopPreview();
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					camera.release();
				} catch (Exception e) {
					e.printStackTrace();
				}
				camera = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		try {
			Camera.Parameters parameters = camera.getParameters();
			try {
				List<Camera.Size> supportedSizes = null;
				// On older devices (<1.6) the following will fail
				// the camera will work nevertheless
				supportedSizes = Compatibility
						.getSupportedPreviewSizes(parameters);

				// preview form factor
				float ff = (float) w / h;
				Log.d("Mixare", "Screen res: w:" + w + " h:" + h
						+ " aspect ratio:" + ff);

				// holder for the best form factor and size
				float bff = 0;
				int bestw = 0;
				int besth = 0;
				Iterator<Camera.Size> itr = supportedSizes.iterator();

				// we look for the best preview size, it has to be the closest
				// to the
				// screen form factor, and be less wide than the screen itself
				// the latter requirement is because the HTC Hero with update
				// 2.1 will
				// report camera preview sizes larger than the screen, and it
				// will fail
				// to initialize the camera
				// other devices could work with previews larger than the screen
				// though
				while (itr.hasNext()) {
					Camera.Size element = itr.next();
					// current form factor
					float cff = (float) element.width / element.height;
					// check if the current element is a candidate to replace
					// the best match so far
					// current form factor should be closer to the bff
					// preview width should be less than screen width
					// preview width should be more than current bestw
					// this combination will ensure that the highest resolution
					// will win
					Log.d("Mixare", "Candidate camera element: w:"
							+ element.width + " h:" + element.height
							+ " aspect ratio:" + cff);
					if (ff - cff <= ff - bff && element.width <= w
							&& element.width >= bestw) {
						bff = cff;
						bestw = element.width;
						besth = element.height;
					}
				}
				Log.d("Mixare", "Chosen camera element: w:" + bestw + " h:"
						+ besth + " aspect ratio:" + bff);
				// Some Samsung phones will end up with bestw and besth = 0
				// because their minimum preview size is bigger then the screen
				// size.
				// In this case, we use the default values: 480x320
				if (bestw == 0 || besth == 0) {
					Log.d("Mixare", "Using default camera parameters!");
					bestw = 480;
					besth = 320;
				}
				parameters.setPreviewSize(bestw, besth);
			} catch (Exception ex) {
				parameters.setPreviewSize(480, 320);
			}

			
			camera.setParameters(parameters);
			
			 if (holder.getSurface() != null) {
				  camera.stopPreview();
			 }
			 
			camera.startPreview();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

class AugmentedView extends View {
	MixView app;
	int xSearch = 200;
	int ySearch = 10;
	int searchObjWidth = 0;
	int searchObjHeight = 0;

	Paint zoomPaint = new Paint();

	public AugmentedView(Context context) {
		super(context);
		
		try {
			app = (MixView) context;

			app.killOnError();
		} catch (Exception ex) {
			app.doError(ex, MixView.GENERAL_ERROR);
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		try {
			// if (app.fError) {
			//
			// Paint errPaint = new Paint();
			// errPaint.setColor(Color.RED);
			// errPaint.setTextSize(16);
			//
			// /*Draws the Error code*/
			// canvas.drawText("ERROR: ", 10, 20, errPaint);
			// canvas.drawText("" + app.fErrorTxt, 10, 40, errPaint);
			//
			// return;
			// }

			app.killOnError();

			MixView.getdWindow().setWidth(canvas.getWidth());
			MixView.getdWindow().setHeight(canvas.getHeight());

			MixView.getdWindow().setCanvas(canvas);

			if (!MixView.getDataView().isInited()) {
				MixView.getDataView().init(MixView.getdWindow().getWidth(),
						MixView.getdWindow().getHeight());
			}
			if (app.isZoombarVisible()) {
				zoomPaint.setColor(Color.WHITE);
				zoomPaint.setTextSize(14);
				String startKM, endKM;
				endKM = "80km";
				startKM = "0km";
				/*
				 * if(MixListView.getDataSource().equals("Twitter")){ startKM =
				 * "1km"; }
				 */
				canvas.drawText(startKM, canvas.getWidth() / 100 * 4,
						canvas.getHeight() / 100 * 85, zoomPaint);
				canvas.drawText(endKM, canvas.getWidth() / 100 * 99 + 25,
						canvas.getHeight() / 100 * 85, zoomPaint);

				int height = canvas.getHeight() / 100 * 85;
				int zoomProgress = app.getZoomProgress();
				if (zoomProgress > 92 || zoomProgress < 6) {
					height = canvas.getHeight() / 100 * 80;
				}
				canvas.drawText(app.getZoomLevel(), (canvas.getWidth()) / 100
						* zoomProgress + 20, height, zoomPaint);
			}

			MixView.getDataView().draw(MixView.getdWindow());
		} catch (Exception ex) {
			app.doError(ex, MixView.GENERAL_ERROR);
		}
	}
}

